---
api_host: "https://vcloud101.macquarieview.com"
api_user: "np_infrasvc@{{ org_name }}"
org_name: "BPAY_Non_Prod_103797_SVC"
catalog_name: "Customer_103797_SVC_Catalog"
vdc_name: "M1SVC28338001"
vdc_storage_profile: "MT-VNX044-T700-PVDC0109"
vapp_tmpl_name: "bpay-rhel7-nv-test"
vm_tmpl_name: "bpay-rhel7-template"
sat_user: templateuser
puppet_user: ansible
ansible_connection: ssh
ansible_ssh_user: ansible
customer_activation_key: "42-rhel7-stg"
tmp_dir: "{{ playbook_dir}}/tmp"
vm_customization_script: |
    #!/bin/bash
    # Guest customization script for BPAY MCS VMs - Non-prod environment only
    #
    # v1.0 20170508 - BM
    # v1.2 20170510 - BM - enable network manager and set static route to satellite
    # v2.0 20170516 - BM - add swap calculation and swap resize
    # v3.0 20170524 - NV - converted for Ansible
    #
    if [ x"$1" == x"precustomization" ]; then

      ## Begin: precustomization
      echo "`date`: This is the precustomization section"
      echo ""
      systemctl enable NetworkManager
      systemctl start NetworkManager
      ## End: precustomization

    elif [ x"$1" == x"postcustomization" ]; then

      ## Begin: set variables
      vmname="{{vm.vm_name}}"
      sat_user="{{sat_user}}"
      sat_pwd="{{sat_pwd}}"
      sat_activation_key="{{customer_activation_key}}"
      mgmt_gateway=`ip -4 a s ens224 | grep inet | awk '{print $4}' | awk -F"." 'GW= $4 - 1 { printf "%d.%d.%d.%d\n",$1,$2,$3,GW }'`
      ## End: set variables

      ## Begin: test variables
      echo "hostname is $vmname"
      echo "sat_user is $sat_user"
      echo "management gateway is $mgmt_gateway"
      ## End: test variables

      ## Begin: postcustomization
      echo "`date`: This is the postcustomization section"
      echo ""
      echo "`date`: executing BPAY customization script for $vmname"
      echo "==================================================="
      echo ""

      echo "Setting DNS settings for $vmname"
      echo "----------------------------------------"
      if [[ $vmname =~ dmz ]]; then
        dns1=10.128.128.25
        dns2=10.128.128.35
      else
        dns1=10.201.9.75
        dns2=10.201.9.85
      fi
      echo "# Setting resolve.conf - VM guest customisation level" > /etc/resolv.conf
      echo "options timeout:2 ndots:2 rotate" >> /etc/resolv.conf
      echo "search ux.bpaylocal app.bpaylocal bpayoob.local win.bpaylocal nw.bpaylocal" >> /etc/resolv.conf
      echo "nameserver $dns1" >> /etc/resolv.conf
      echo "nameserver $dns2" >> /etc/resolv.conf
      echo "Done - Resolv.conf"
      cat /etc/resolv.conf
      echo ""

      echo "Resizing swap space"
      echo "-------------------"
      old_swap=`swapon --bytes --show=size --noheadings | awk '{ print int($1 / 1024000) }'`
      memory=`free -m | grep "^Mem:" | awk '{ print $2 }'`
      free_vg=`vgdisplay sysvg --units m | grep "Free.*PE" | awk '{ print int($(NF-1)) }'`
      ## Calculate memory
      if [ $memory -lt 2048 ]; then
        ## round off to upper 512MB block and double the size
        new_swap=`expr \( $memory / 512 + 1 \) \* 512 \* 2`
      elif [ $memory -lt 8192 ]; then
        ## round off to upper 512MB block
        new_swap=`expr \( $memory / 512 + 1 \) \* 512`
      else
        new_swap=8192
      fi
      ## Check proposed swap size and extend if required
      if [[ $new_swap -gt $old_swap ]]; then
        if [[ $free_vg -gt $new_swap ]]; then
          ## Extend swap
          /sbin/swapoff -v /dev/sysvg/lv_swap
          /sbin/lvextend -L ${new_swap}M /dev/sysvg/lv_swap
          /sbin/mkswap /dev/sysvg/lv_swap
          /sbin/swapon /dev/sysvg/lv_swap
        fi
      fi
      echo ""

      echo "Creating user...."
      echo "---------------------"
      useradd -m -p $(echo "{{ansible_ssh_pass}}" | openssl passwd -1 -stdin) {{ansible_ssh_user}}
      echo -e "{{ansible_ssh_user}} \t ALL=(ALL) \t ALL" >> /etc/sudoers

      echo "Updating TCP wrapper...."
      echo "---------------------"
      sed -ie '/^sshd/ s/$/, 10.27.192.250/' /etc/hosts.allow
      sed -ie '/^AllowGroups/ s/$/ ansible/' /etc/ssh/sshd_config
      systemctl restart sshd

      echo "Prevent NetworkManager from overwriting DNS resolv.conf"
      echo "--------------------------------------------------------"
      echo "/bin/sed -i '/[main]/a dns=none' /etc/NetworkManager/NetworkManager.conf"
      echo "/bin/systemctl start NetworkManager"
      /bin/sed -i '/^\[main\]/a dns=none' /etc/NetworkManager/NetworkManager.conf
      /bin/systemctl enable NetworkManager
      /bin/systemctl start NetworkManager
      echo ""

      echo "Set static route for RedHat Satellite Server"
      echo "--------------------------------------------"
      echo "/usr/bin/nmcli c m ens224 ipv4.gateway $mgmt_gateway"
      /usr/bin/nmcli c m ens224 ipv4.routes "10.117.1.245/32 $mgmt_gateway"
      /usr/bin/nmcli c r
      /usr/bin/nmcli c u ens224
      echo ""

      echo "Registering to MCS satellite for $vmname"
      echo "----------------------------------------"
      echo "rhnreg_ks --profilename=$vmname --username=$sat_user --password=$sat_pwd --activationkey=$sat_activation_key --force"
      rhnreg_ks --profilename=$vmname --username=$sat_user --password=$sat_pwd --activationkey=$sat_activation_key --force
      echo ""

      echo "Installing Puppet agent for $vmname"
      echo "-----------------------------------"
      echo "curl http://repository.app.bpaylocal/software/Unix/Puppet/bpay/2016.1.2/el-7-x86_64.bash | bash"
      curl http://repository.app.bpaylocal/software/Unix/Puppet/bpay/2016.1.2/el-7-x86_64.bash | bash
      echo ""

      echo "Clean-up sysconfig setup scripts for $vmname"
      rm -f /root/firstrun
      rm -f /root/sysunconfig.sh
      rm -f /root/sysconfig_cli.sh
      cp -p /etc/issue /etc/motd
      echo ""

      echo "==================================================="
      echo "`date`: BPAY customization script done for $vmname"
      ## End: postcustomization

    fi
